// src/components/HomePage.js
// src/pages/HomePage.js
import React from 'react';
import MainLayout from '../components/layouts/MainLayout'; // Corrected path
import { Link } from 'react-router-dom';

const HomePage = () => {
  return (
    <MainLayout headerText="Jon Nicholas Puzzles">
      <div>
        <h2>Select a puzzle type to get started:</h2>
        <ul>
          <li><Link to="/buranku">BURANKU</Link></li>
          <li><Link to="/katachi">Katachi</Link></li>
          <li><Link to="/the-spider">The Spider</Link></li>
          <li><Link to="/hidden-maze">Hidden Maze</Link></li>
        </ul>
      </div>
    </MainLayout>
  );
};

export default HomePage;
