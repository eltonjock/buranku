import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import styled from 'styled-components';
import HomePage from './pages/HomePage';
import BurankuList from './components/puzzles/buranku/BurankuList';
import BurankuPuzzlePage from './components/puzzles/buranku/BurankuPuzzlePage';
import BurankuSolutionPage from './components/puzzles/buranku/BurankuSolutionPage';
import Admin from './admin/Admin';
import GlobalStyles from './globalStyles';
import ProtectedRoute from './admin/ProtectedRoute';
import LoginPage from './admin/LoginPage';

const AppWrapper = styled.div`
  text-align: center;
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  max-width: 100%;
  padding: 0px;
  box-sizing: border-box;
  border: 0px solid white;
`;

function App() {
  return (
    <Router>
      <GlobalStyles />
      <AppWrapper>
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/buranku" element={<BurankuList />} />
          <Route path="/buranku/:version" element={<BurankuPuzzlePage />} />
          <Route path="/buranku/solution/:version" element={<BurankuSolutionPage />} />
          <Route path="/login" element={<LoginPage />} />
          <Route path="/admin" element={<ProtectedRoute><Admin /></ProtectedRoute>} />
          <Route path="/admin/:puzzleType/:versionNumber" element={<ProtectedRoute><BurankuAdmin /></ProtectedRoute>} />
        </Routes>
      </AppWrapper>
    </Router>
  );
}

export default App;
