// src/firebase-config.js
import { initializeApp } from 'firebase/app';
import { getFirestore } from 'firebase/firestore';

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDryU63mRj1q33cBoBRBi63h_sI1tjX2bk",
  authDomain: "dads-puzzles.firebaseapp.com",
  projectId: "dads-puzzles",
  storageBucket: "dads-puzzles.appspot.com",
  messagingSenderId: "294594233854",
  appId: "1:294594233854:web:f3d13397405f95ab906caf",
  measurementId: "G-HH4PB3R7LP"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

// Initialize Firestore
const db = getFirestore(app);

export { db };
