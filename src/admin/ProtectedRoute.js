import React from 'react';
import { Navigate } from 'react-router-dom';
import netlifyIdentity from 'netlify-identity-widget';

const ProtectedRoute = ({ children }) => {
  const isLoggedIn = netlifyIdentity.currentUser();

  if (!isLoggedIn) {
    return <Navigate to="/login" replace />;
  }

  return children;
};

export default ProtectedRoute;
