// BurankuAdminPuzzle.js
import React from 'react';
import styled from 'styled-components';

const GridContainer = styled.div`
  position: absolute;
  top: 0;
  left: 50%;
  transform: translateX(-50%);
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0;
  box-sizing: border-box;
`;

const Waffle = styled.div`
  display: grid;
  grid-template-columns: 3fr repeat(${props => props.gridSize - 1}, 1fr);
  grid-template-rows: 3fr repeat(${props => props.gridSize - 1}, 1fr);
  width: 100%;
  height: 100%;
  border-collapse: collapse;
`;

const Cell = styled.div`
  position: relative;
  text-align: center;
  vertical-align: middle;
  border: ${props =>
    props.rowIndex === 0 || props.colIndex === 0 || props.rowIndex === props.gridSize - 1 || props.colIndex === props.gridSize - 1
      ? 'none'
      : '1px solid black'};
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 1rem;
  cursor: pointer;  // Ensure that all cells are interactive unless specified otherwise
`;

const BurankuAdminPuzzle = ({ grid, setGrid, gridSize }) => {
  const handleClick = (row, col) => {
    if (row === 0 || col === 0 || row === grid.length - 1 || col === grid[0].length - 1) return;  // Ignore border clicks

    const newGrid = grid.map((rowArr, rowIndex) =>
      rowArr.map((cellColor, colIndex) => {
        if (rowIndex === row && colIndex === col) {
          return cellColor === 'white' ? '#e0e0e0' : cellColor === '#e0e0e0' ? 'black' : 'white'; // Toggle cell color
        }
        return cellColor;
      })
    );
    setGrid(newGrid);
  };

  return (
    <GridContainer>
      <Waffle gridSize={gridSize}>
        {grid.map((row, rowIndex) =>
          row.map((color, colIndex) => (
            <Cell
              key={`${rowIndex}-${colIndex}`}
              rowIndex={rowIndex}
              colIndex={colIndex}
              gridSize={gridSize}
              style={{ backgroundColor: color }}
              onClick={() => handleClick(rowIndex, colIndex)}
            />
          ))
        )}
      </Waffle>
    </GridContainer>
  );
};

export default BurankuAdminPuzzle;
