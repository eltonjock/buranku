// BurankuAdmin.js
import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { doc, getDoc, setDoc } from 'firebase/firestore';
import { db } from './firebase-config';
import BurankuAdminPuzzle from './BurankuAdminPuzzle';
import netlifyIdentity from 'netlify-identity-widget';

const BurankuAdmin = () => {
  const { versionNumber, puzzleType } = useParams();
  const [gridSize, setGridSize] = useState(10);
  const [grid, setGrid] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    netlifyIdentity.init();
    console.log('Netlify Identity initialized');
    console.warn('Netlify Identity initialized (warning)');
    console.error('Netlify Identity initialized (error)');
  }, []);

  useEffect(() => {
    const fetchGridSize = async () => {
      const versionRef = doc(db, 'puzzles', puzzleType, 'versions', versionNumber);
      console.log('Fetching grid size for', puzzleType, 'version', versionNumber);
      console.warn('Fetching grid size (warning)', puzzleType, versionNumber);
      console.error('Fetching grid size (error)', puzzleType, versionNumber);
      try {
        const docSnap = await getDoc(versionRef);
        console.log('Document snapshot:', docSnap);
        console.warn('Document snapshot (warning):', docSnap);
        console.error('Document snapshot (error):', docSnap);
        if (docSnap.exists() && docSnap.data().gridSize) {
          const fetchedGridSize = docSnap.data().gridSize;
          console.log('Fetched grid size:', fetchedGridSize);
          console.warn('Fetched grid size (warning):', fetchedGridSize);
          console.error('Fetched grid size (error):', fetchedGridSize);
          setGridSize(fetchedGridSize);
        } else {
          console.log('Document does not exist, creating new document with default grid size');
          console.warn('Document does not exist (warning), creating new document with default grid size');
          console.error('Document does not exist (error), creating new document with default grid size');
          await setDoc(versionRef, { gridSize: 10 });
          setGridSize(10);
        }
        setLoading(false);
      } catch (error) {
        console.error('Failed to fetch grid size:', error);
        setError('Failed to fetch grid size. Please try again.');
        setLoading(false);
      }
    };
    fetchGridSize();
  }, [versionNumber, puzzleType]);

  useEffect(() => {
    console.log('Initializing grid with size', gridSize);
    console.warn('Initializing grid with size (warning)', gridSize);
    console.error('Initializing grid with size (error)', gridSize);
    const initialGrid = Array.from({ length: gridSize }, () =>
      Array.from({ length: gridSize }, () => 'white')
    );
    setGrid(initialGrid);
  }, [gridSize]);

  if (loading) {
    return <p>Loading...</p>;
  }

  if (error) {
    return <p>{error}</p>;
  }

  return (
    <div>
      <h1>{puzzleType.toUpperCase()} Puzzle - Version {versionNumber}</h1>
      <BurankuAdminPuzzle grid={grid} setGrid={setGrid} gridSize={gridSize} />
    </div>
  );
};

export default BurankuAdmin;