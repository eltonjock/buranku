import React from 'react';
import { useNavigate } from 'react-router-dom';
import netlifyIdentity from 'netlify-identity-widget';

const LoginPage = () => {
  const navigate = useNavigate();

  const handleLogin = () => {
    netlifyIdentity.open('login');
  };

  netlifyIdentity.on('login', () => {
    const user = netlifyIdentity.currentUser();
    if (user) {
      navigate('/admin');
    }
  });

  return (
    <div>
      <h1>Login Page</h1>
      <button onClick={handleLogin}>Login with Netlify Identity</button>
    </div>
  );
};

export default LoginPage;