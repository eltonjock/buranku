// Admin.js
import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { collection, getDocs, doc, setDoc } from 'firebase/firestore';
import { db } from '../firebase-config';
import netlifyIdentity from 'netlify-identity-widget';
import ProtectedRoute from './ProtectedRoute';

const Admin = () => {
  const navigate = useNavigate();
  const [puzzleTypes, setPuzzleTypes] = useState([]);
  const [selectedPuzzleType, setSelectedPuzzleType] = useState('');
  const [versionNumber, setVersionNumber] = useState('');
  const [gridSize, setGridSize] = useState('');
  const [errorMessage, setErrorMessage] = useState('');

  useEffect(() => {
    netlifyIdentity.init();
    const fetchPuzzleTypes = async () => {
      try {
        const querySnapshot = await getDocs(collection(db, 'puzzles'));
        setPuzzleTypes(querySnapshot.docs.map(doc => doc.id));
      } catch (error) {
        console.error('Failed to fetch puzzle types:', error);
        setErrorMessage('Failed to fetch puzzle types. Please try again.');
      }
    };
    fetchPuzzleTypes();
  }, []);

  const handleEnter = async () => {
    if (!versionNumber || !gridSize) {
      setErrorMessage('Both fields must be filled.');
      return;
    }
    const versionRef = doc(db, 'puzzles', selectedPuzzleType, 'versions', versionNumber);
    try {
      await setDoc(versionRef, { gridSize: parseInt(gridSize) });
      navigate(`/admin/${selectedPuzzleType.toLowerCase()}/${versionNumber}`);
    } catch (error) {
      console.error('Failed to create version document:', error);
      setErrorMessage('Failed to create version document. Please try again.');
    }
  };

  return (
    <ProtectedRoute>
      <div>
        <h1>Admin Dashboard</h1>
        <select value={selectedPuzzleType} onChange={e => setSelectedPuzzleType(e.target.value)}>
          <option value="">Select a puzzle type</option>
          {puzzleTypes.map(type => <option key={type} value={type}>{type}</option>)}
        </select>
        {selectedPuzzleType && (
          <>
            <input type="text" value={versionNumber} onChange={e => setVersionNumber(e.target.value)} placeholder="Version Number" />
            <input type="text" value={gridSize} onChange={e => setGridSize(e.target.value)} placeholder="Grid Size" />
            <button onClick={handleEnter}>ENTER</button>
            {errorMessage && <p>{errorMessage}</p>}
          </>
        )}
      </div>
    </ProtectedRoute>
  );
};

export default Admin;