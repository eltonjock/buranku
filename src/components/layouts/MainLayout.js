// src/components/layouts/MainLayout.js
import React from 'react';
import styled from 'styled-components';
import Header from '../common/Header';
import Footer from '../common/Footer';
import MenuButton from '../common/MenuButton';

const LayoutWrapper = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
`;

const MainContent = styled.main`
  flex: 1;
  padding: 0px;
`;

const MainLayout = ({ children, headerText }) => {
  return (
    <LayoutWrapper>
      <Header text={headerText} />
      <MenuButton />
      <MainContent>{children}</MainContent>
      <Footer />
    </LayoutWrapper>
  );
};

export default MainLayout;