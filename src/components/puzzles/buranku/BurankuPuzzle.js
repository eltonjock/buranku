import React from 'react';
import styled from 'styled-components';

const GridContainer = styled.div`
  position: absolute;
  top: 0;
  left: 50%;
  transform: translateX(-50%);
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0;
  box-sizing: border-box;
`;

const Waffle = styled.div`
  display: grid;
  grid-template-columns: 3fr repeat(${props => props.gridSize - 1}, 1fr);
  grid-template-rows: 3fr repeat(${props => props.gridSize - 1}, 1fr);
  width: 100%;
  height: 100%;
  border-collapse: collapse;
`;

const Cell = styled.div`
  position: relative;
  text-align: center;
  vertical-align: middle;
  border: ${props =>
    props.rowIndex === 0 || props.colIndex === 0 || props.rowIndex === props.gridSize - 1 || props.colIndex === props.gridSize - 1
      ? 'none'
      : '1px solid black'};
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 1rem;
  cursor: ${props =>
    props.rowIndex === 0 || props.colIndex === 0 || props.rowIndex === props.gridSize - 1 || props.colIndex === props.gridSize - 1
      ? 'default'
      : 'pointer'};
`;

const RowCell = styled(Cell)`
  grid-column: 1 / span 1;
  justify-content: flex-end;
  padding-right: 5px;
`;

const ColumnCell = styled(Cell)`
  grid-row: 1 / span 1;
  align-items: flex-end;
  padding-bottom: 5px;
`;

const ClueCellColumn = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  height: 100%;
  padding-right: 5px;
  box-sizing: border-box;
  font-size: 1rem;
`;

const ClueCellRow = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  align-items: center;
  height: 100%;
  padding-bottom: 5px;
  box-sizing: border-box;
  font-size: 1rem;
`;

const BurankuPuzzle = ({ grid, setGrid, clues, readOnly }) => {
  const handleClick = (row, col) => {
    if (readOnly || row === 0 || col === 0 || row === grid.length - 1 || col === grid[0].length - 1) return;

    const newGrid = grid.map((rowArr, rowIndex) =>
      rowArr.map((cellColor, colIndex) => {
        if (rowIndex === row && colIndex === col) {
          return cellColor === 'white' ? '#e0e0e0' : cellColor === '#e0e0e0' ? 'black' : 'white';
        }
        return cellColor;
      })
    );
    setGrid(newGrid);
  };

  const renderClue = (rowIndex, colIndex) => {
    const clue = clues.find(
      clue => (clue.c === 0 && clue.r === rowIndex) || // For column clues
        (clue.r === 0 && clue.c === colIndex - 1) // For row clues, subtract 1 from colIndex
    );

    if (clue) {
      return clue.r === 0 ? (
        <div dangerouslySetInnerHTML={{ __html: clue.n }} />
      ) : (
        clue.n
      );
    }
    return '';
  };

  return (
    <GridContainer>
      <Waffle gridSize={grid.length}>
        {grid.map((row, rowIndex) =>
          row.map((color, colIndex) => (
            rowIndex === 0 && colIndex === 0 ? null : (
              colIndex === 0 ? (
                <RowCell
                  key={`${rowIndex}-${colIndex}`}
                  rowIndex={rowIndex}
                  colIndex={colIndex}
                  gridSize={grid.length}
                  onClick={() => handleClick(rowIndex, colIndex)}
                >
                  {rowIndex !== 0 && (
                    <ClueCellColumn>
                      {renderClue(rowIndex, colIndex)}
                    </ClueCellColumn>
                  )}
                </RowCell>
              ) : rowIndex === 0 ? (
                <ColumnCell
                  key={`${rowIndex}-${colIndex}`}
                  rowIndex={rowIndex}
                  colIndex={colIndex}
                  gridSize={grid.length}
                  onClick={() => handleClick(rowIndex, colIndex)}
                >
                  <ClueCellRow>
                    {renderClue(rowIndex, colIndex)}
                  </ClueCellRow>
                </ColumnCell>
              ) : (
                <Cell
                  key={`${rowIndex}-${colIndex}`}
                  rowIndex={rowIndex}
                  colIndex={colIndex}
                  gridSize={grid.length}
                  style={{ backgroundColor: color }}
                  onClick={() => handleClick(rowIndex, colIndex)}
                />
              )
            )
          ))
        )}
      </Waffle>
    </GridContainer>
  );
};

export default BurankuPuzzle;
