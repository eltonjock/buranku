import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { doc, getDoc } from 'firebase/firestore';
import { db } from '../../../firebase-config';
import MainLayout from '../../layouts/MainLayout';
import BurankuInstructions from './BurankuInstructions';
import CloseWindowButton from '../../common/CloseWindowButton';
import BurankuSolution from './BurankuSolution';
import styled from 'styled-components';

const BurankuPuzzlePageWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  max-width: 100%;
  padding: 0px;
  box-sizing: border-box;
`;

const LoadingText = styled.div`
  font-size: 18px;
  font-weight: bold;
  margin-top: 0px;
`;

const PuzzleWrapper = styled.div`
  display: flex;
  justify-content: center;
  width: 100%;
  padding: 0px;
  box-sizing: border-box;
`;

const PuzzleContainer = styled.div`
  position: relative;
  width: 100%;
  max-width: 100%;
  margin: 0 auto;
  margin-bottom: 0px;
  box-sizing: border-box;

  &::before {
    content: "";
    display: block;
    padding-top: 100%;
  }
`;

function BurankuSolutionPage() {
  const { version } = useParams();
  const [puzzleData, setPuzzleData] = useState(null);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    setIsLoading(true);
    const docRef = doc(db, 'puzzles', 'BURANKU', 'versions', version);
    getDoc(docRef).then(docSnap => {
      if (docSnap.exists()) {
        const data = docSnap.data();
        setPuzzleData(data);
      } else {
        console.error("No such document exists for version:", version);
      }
      setIsLoading(false);
    }).catch(error => {
      console.error("Error fetching document:", error);
      setIsLoading(false);
    });
  }, [version]);

  if (isLoading) {
    return (
      <MainLayout headerText={`Loading Puzzle #${version}...`}>
        <LoadingText>Loading puzzle data...</LoadingText>
      </MainLayout>
    );
  }

  if (!puzzleData || !puzzleData.clues || !puzzleData.answers || typeof puzzleData.gridSize === 'undefined') {
    return (
      <MainLayout headerText="Data not found">
        <LoadingText>Data not found or is incomplete for version: {version}</LoadingText>
      </MainLayout>
    );
  }

  return (
    <MainLayout headerText={`Solution for Puzzle #${version}`}>
      <BurankuPuzzlePageWrapper>
        <BurankuInstructions />
        <PuzzleWrapper>
          <PuzzleContainer>
            <BurankuSolution 
              gridSize={puzzleData.gridSize} 
              clues={puzzleData.clues} 
              answers={puzzleData.answers} />
          </PuzzleContainer>
        </PuzzleWrapper>
        <CloseWindowButton />
      </BurankuPuzzlePageWrapper>
    </MainLayout>
  );
}

export default BurankuSolutionPage;
