// BurankuInstructions.js
import React from 'react';
import styled from 'styled-components';

const InstructionsWrapper = styled.div`
  margin-bottom: 30px;
  margin-top: 30px;
  margin-left: 30px;
  margin-right: 30px;
`;

const InstructionsText = styled.p`
  margin: 0;
  line-height: 1.5;
`;

function BurankuInstructions() {
  return (
    <InstructionsWrapper>
      <InstructionsText>
        In the grid below, blacken in squares to make a circuitous, unbroken path.<br />
        Click on the squares to turn them grey (to keep track of blank ones) and again to turn them black (as a part of the pathway).<br />
        Numbers are provided to indicate how many consecutive blank squares are in some of the columns and rows.<br />
        The solution is unique.<br /><br />
        Rules:<br />
        1. Black squares must share only two sides with other black squares.<br />
        2. There can be no empty 2X2 areas.
        <br />
      </InstructionsText>
    </InstructionsWrapper>
  );
}

export default BurankuInstructions;