// src/components/puzzles/buranku/BurankuControlButtons.js
import React from 'react';
import styled from 'styled-components';

const ButtonContainer = styled.div`
  margin-top: 30px;
  margin-bottom: 30px;
  display: flex;
  justify-content: center;
`;

const Button = styled.button`
  background-color: #282c34;
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  cursor: pointer;
  transition-duration: 0.4s;

  &:hover {
    background-color: #808080;
  }
`;

function BurankuControlButtons({ onReset, onSubmit, onShowSolution, onCloseSolution, isSolutionWindowOpen }) {
  const handleResetClick = () => {
    if (window.confirm('Are you sure you want to reset the puzzle?')) {
      onReset();
    }
  };

  return (
    <ButtonContainer>
      <Button onClick={handleResetClick}>RESET</Button>
      <Button onClick={onSubmit}>SUBMIT</Button>
      {!isSolutionWindowOpen && <Button onClick={onShowSolution}>SOLUTION</Button>}
      {isSolutionWindowOpen && <Button onClick={onCloseSolution}>SOLUTION</Button>}
    </ButtonContainer>
  );
}

export default BurankuControlButtons;