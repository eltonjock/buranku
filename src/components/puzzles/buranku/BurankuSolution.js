import React from 'react';
import styled from 'styled-components';

const GridContainer = styled.div`
  position: absolute;
  top: 0;
  left: 50%;
  transform: translateX(-50%);
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0;
  box-sizing: border-box;
`;

const Waffle = styled.div`
  display: grid;
  grid-template-columns: 3fr repeat(${props => props.gridSize - 1}, 1fr);
  grid-template-rows: 3fr repeat(${props => props.gridSize - 1}, 1fr);
  width: 100%;
  height: 100%;
  border-collapse: collapse;
`;

const Cell = styled.div`
  position: relative;
  text-align: center;
  vertical-align: middle;
  border: ${props =>
    props.rowIndex === 0 || props.colIndex === 0 || props.rowIndex === props.gridSize - 1 || props.colIndex === props.gridSize - 1
      ? 'none'
      : '1px solid black'};
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 1rem;
`;

const BurankuSolution = ({ gridSize, clues, answers }) => {
  const grid = Array.from({ length: gridSize }, () =>
    Array.from({ length: gridSize }, () => 'white'));

  if (answers && Array.isArray(answers)) {
    answers.forEach(answer => {
      if (answer.rowIndex !== null && answer.colIndex !== null) {
        grid[answer.rowIndex][answer.colIndex] = 'black';
      }
    });
  }

  const renderClue = (rowIndex, colIndex) => {
    const clue = clues.find(clue => {
      return (clue.c === 0 && clue.r === rowIndex) || (clue.r === 0 && clue.c === colIndex);
    });

    if (clue) {
      return clue.r === 0 ? <div dangerouslySetInnerHTML={{ __html: clue.n }} /> : clue.n;
    }
    return '';
  };

  return (
    <GridContainer>
      <Waffle gridSize={gridSize}>
        {grid.map((row, rowIndex) =>
          row.map((cellColor, colIndex) => (
            <Cell
              key={`${rowIndex}-${colIndex}`}
              rowIndex={rowIndex}
              colIndex={colIndex}
              gridSize={gridSize}
              style={{ backgroundColor: cellColor }}
            >
              {(rowIndex === 0 || colIndex === 0) && renderClue(rowIndex, colIndex)}
            </Cell>
          ))
        )}
      </Waffle>
    </GridContainer>
  );
};

export default BurankuSolution;
