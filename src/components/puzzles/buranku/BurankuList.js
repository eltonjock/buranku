import React, { useState, useEffect } from 'react';
import { collection, getDocs } from 'firebase/firestore';
import { db } from '../../../firebase-config'; // Make sure this path correctly points to your firebase-config file
import { Link } from 'react-router-dom';
import MainLayout from '../../layouts/MainLayout';

const BurankuList = () => {
    const [versions, setVersions] = useState([]);

    useEffect(() => {
        const fetchVersions = async () => {
            // Correctly referencing the 'versions' subcollection
            const versionsRef = collection(db, 'puzzles', 'BURANKU', 'versions');
            const querySnapshot = await getDocs(versionsRef);
            const loadedVersions = querySnapshot.docs.map(doc => ({
                id: doc.id,
                ...doc.data(),
            }));
            setVersions(loadedVersions);
        };

        fetchVersions();
    }, []);

    return (
        <MainLayout headerText="Buranku Puzzles">
            <div>
                <h2>Available BURANKU Versions:</h2>
                <ul>
                    {versions.map(version => (
                        <li key={version.id}>
                            <Link to={`/buranku/${version.id}`}>Buranku Puzzle #{version.versionNumber}</Link>
                        </li>
                    ))}
                </ul>
            </div>
        </MainLayout>
    );
};

export default BurankuList;
