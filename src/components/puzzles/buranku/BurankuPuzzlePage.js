import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import styled from 'styled-components';
import { db } from '../../../firebase-config';
import { doc, getDoc } from 'firebase/firestore';
import { isEqual } from 'lodash';
import MainLayout from '../../layouts/MainLayout';
import BurankuInstructions from './BurankuInstructions';
import BurankuControlButtons from './BurankuControlButtons';
import BurankuPuzzle from './BurankuPuzzle';

const BurankuPuzzlePageWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  max-width: 100%;
  padding: 0px;
  box-sizing: border-box;
`;

const LoadingText = styled.div`
  font-size: 18px;
  font-weight: bold;
  margin-top: 20px;
`;

const PuzzleWrapper = styled.div`
  display: flex;
  justify-content: center;
  width: 100%;
  padding: 0px;
  box-sizing: border-box;
`;

const PuzzleContainer = styled.div`
  position: relative;
  width: 100%;
  max-width: 100%;
  margin: 0 auto;
  margin-bottom: 20px;
  box-sizing: border-box;

  &::before {
    content: "";
    display: block;
    padding-top: 100%; // Maintains aspect ratio
  }
`;

function BurankuPuzzlePage() {
  const { version } = useParams();
  const [puzzleData, setPuzzleData] = useState(null);
  const [grid, setGrid] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true); // Start loading
      const docRef = doc(db, 'puzzles', 'BURANKU', 'versions', version);
      const docSnap = await getDoc(docRef);

      if (docSnap.exists()) {
        const data = docSnap.data();
        setPuzzleData(data);
        console.log('Puzzle data:', data);
        // Attempt to restore grid state from localStorage
        const savedGrid = JSON.parse(localStorage.getItem(`burankuPuzzle-${version}`));
        if (savedGrid && savedGrid.length === data.gridSize) {
          setGrid(savedGrid);
        } else {
          // Fallback to initializing grid
          setGrid(Array.from({ length: data.gridSize }, () => Array.from({ length: data.gridSize }, () => 'white')));
        }
      } else {
        console.log("No such document!");
      }
      setIsLoading(false); // End loading
    };

    fetchData();
  }, [version]);

  useEffect(() => {
    // Persist grid state to localStorage upon grid change
    if (grid.length > 0) {
      localStorage.setItem(`burankuPuzzle-${version}`, JSON.stringify(grid));
    }
  }, [grid, version]);

  const handleReset = () => {
    if (window.confirm('Are you sure you want to reset the puzzle?')) {
      setGrid(Array.from({ length: puzzleData.gridSize }, () => Array.from({ length: puzzleData.gridSize }, () => 'white')));
    }
  };

  const handleSubmit = () => {
    const userMarkedCells = new Set();
    grid.forEach((row, rowIndex) => {
      row.forEach((cell, colIndex) => {
        if (cell === 'black') {
          userMarkedCells.add(`${rowIndex}-${colIndex}`);
        }
      });
    });

    const correctCells = new Set(puzzleData.answers.map(answer => `${answer.rowIndex}-${answer.colIndex}`));

    console.log('User marked cells:', userMarkedCells);
    console.log('User marked cells size:', userMarkedCells.size);
    console.log('Correct cells:', correctCells);
    console.log('Correct cells size:', correctCells.size);

    const isCorrectSolution = isEqual(userMarkedCells, correctCells);

    console.log('Is correct solution:', isCorrectSolution);

    if (isCorrectSolution) {
      alert('Congratulations! You have correctly solved the puzzle.');
    } else {
      alert('Some cells are incorrect. Please try again.');
    }
  };

  if (isLoading) {
    return (
      <MainLayout headerText={`Loading Puzzle #${version}...`}>
        <LoadingText>Loading puzzle data...</LoadingText>
      </MainLayout>
    );
  }

  return (
    <MainLayout headerText={`Buranku Puzzle #${version}`}>
      <BurankuPuzzlePageWrapper>
        <BurankuInstructions />
        <PuzzleWrapper>
          <PuzzleContainer>
            <BurankuPuzzle grid={grid} setGrid={setGrid} clues={puzzleData.clues} readOnly={false} />
          </PuzzleContainer>
        </PuzzleWrapper>
        <BurankuControlButtons onReset={handleReset} onSubmit={handleSubmit} onShowSolution={() => window.open(`/buranku/solution/${version}`, '_blank')} />
      </BurankuPuzzlePageWrapper>
    </MainLayout>
  );
}

export default BurankuPuzzlePage;