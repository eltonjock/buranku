// src/components/HiddenMazeList.js
import React, { useState, useEffect } from 'react';
import MainLayout from './MainLayout';
import { Link } from 'react-router-dom';
import { collection, getDocs } from 'firebase/firestore';
import { db } from '../firebase-config';

const HiddenMazeList = () => {
  const [versions, setVersions] = useState([]);

  useEffect(() => {
    const fetchVersions = async () => {
      // Adjust Firestore path as needed
      const querySnapshot = await getDocs(collection(db, "HIDDEN-MAZE", "versions"));
      const loadedVersions = querySnapshot.docs.map(doc => doc.id);
      setVersions(loadedVersions);
    };

    fetchVersions();
  }, []);

  return (
    <MainLayout headerText="Hidden Maze Puzzles">
      <div>
        <h2>Available Hidden Maze Versions:</h2>
        <ul>
          {versions.map(version => (
            <li key={version}>
              <Link to={`/puzzle/hidden-maze/${version}`}>Hidden Maze Puzzle #{version}</Link>
            </li>
          ))}
        </ul>
      </div>
    </MainLayout>
  );
};

export default HiddenMazeList;

