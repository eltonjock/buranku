// src/components/KatachiList.js
import React, { useState, useEffect } from 'react';
import MainLayout from './MainLayout';
import { Link } from 'react-router-dom';
import { collection, getDocs } from 'firebase/firestore';
import { db } from '../firebase-config';

const KatachiList = () => {
  const [versions, setVersions] = useState([]);

  useEffect(() => {
    const fetchVersions = async () => {
      // Adjust Firestore path as needed
      const querySnapshot = await getDocs(collection(db, "KATACHI", "versions"));
      const loadedVersions = querySnapshot.docs.map(doc => doc.id);
      setVersions(loadedVersions);
    };

    fetchVersions();
  }, []);

  return (
    <MainLayout headerText="Katachi Puzzles">
      <div>
        <h2>Available Katachi Versions:</h2>
        <ul>
          {versions.map(version => (
            <li key={version}>
              <Link to={`/puzzle/katachi/${version}`}>Katachi Puzzle #{version}</Link>
            </li>
          ))}
        </ul>
      </div>
    </MainLayout>
  );
};

export default KatachiList;

