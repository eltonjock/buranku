import React from 'react';
import styled from 'styled-components';

const FooterWrapper = styled.footer`
  background-color: #282c34;
  color: white;
  text-align: center;
  padding: 20px;
`;

const FooterText = styled.p`
  margin: 20;
`;

function Footer() {
  return (
    <FooterWrapper>
      <FooterText>© 2024 JON NICHOLAS</FooterText>
      <FooterText>All Rights Reserved</FooterText>
    </FooterWrapper>
  );
}

export default Footer;