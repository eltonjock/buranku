// CloseWindowButton.js
import React from 'react';
import styled from 'styled-components';

const ButtonContainer = styled.div`
  margin-top: 0px;
  margin-bottom: 0px;
  display: flex;
  justify-content: center;
`;

const Button = styled.button`
  background-color: #282c34;
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  cursor: pointer;
  transition-duration: 0.4s;

  &:hover {
    background-color: #808080;
  }
`;

const CloseWindowButton = () => {
  return (
    <ButtonContainer>
      <Button onClick={() => window.close()}>CLOSE WINDOW</Button>
    </ButtonContainer>
  );
};

export default CloseWindowButton;
