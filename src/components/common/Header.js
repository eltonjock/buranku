// src/components/Header.js
import React from 'react';
import styled from 'styled-components';

const HeaderWrapper = styled.header`
  background-color: #282c34;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 20px;
`;

const HeaderTitle = styled.h1`
  color: white;
  font-size: 24px;
  margin: 0;
`;

function Header({ text }) {
  return (
    <HeaderWrapper>
      <HeaderTitle>{text}</HeaderTitle>
    </HeaderWrapper>
  );
}

export default Header;