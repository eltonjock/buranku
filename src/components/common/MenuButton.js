// src/MenuButton.js
import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

const MenuButtonWrapper = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  z-index: 1000;
  background: #fff;
  padding: 10px;
`;

const MenuLink = styled(Link)`
  color: #000;
  text-decoration: none;
`;

const MenuButton = () => {
  return (
    <MenuButtonWrapper>
      <MenuLink to="/">Home</MenuLink>
      {/* Add additional menu items here */}
    </MenuButtonWrapper>
  );
};

export default MenuButton;