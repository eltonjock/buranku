// src/PuzzleList.js
import React, { useState, useEffect } from 'react';
import { db } from './firebase-config';
import { Link } from 'react-router-dom';

const PuzzleList = () => {
  const [puzzles, setPuzzles] = useState([]);

  useEffect(() => {
    const getPuzzles = async () => {
      const snapshot = await db.collection('puzzles').doc('BURANKU').collection('versions').get();
      const puzzlesData = snapshot.docs.map(doc => ({
        id: doc.id,
        ...doc.data()
      }));
      setPuzzles(puzzlesData);
    };

    getPuzzles();
  }, []);

  return (
    <div>
      <h2>Available Puzzles</h2>
      <ul>
        {puzzles.map(puzzle => (
          <li key={puzzle.id}>
            <Link to={`/puzzle/${puzzle.versionNumber}`}>Puzzle {puzzle.versionNumber}</Link>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default PuzzleList;

