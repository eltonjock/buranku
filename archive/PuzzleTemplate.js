import React from 'react';
import Instructions from './Instructions';
import ControlButtons from './ControlButtons';

const PuzzleTemplate = ({ puzzle, puzzleComponent, onReset, onSubmit, onShowSolution, viewSolution, handleCloseSolution }) => {
  return (
    <div className="App">
      {!viewSolution && <Instructions />}
      <div className="puzzle-wrapper">
        <div className="puzzle-container">
          {puzzleComponent}
        </div>
      </div>
      <div className="buttons-container">
        {!viewSolution && <ControlButtons onReset={onReset} onSubmit={onSubmit} onShowSolution={onShowSolution} />}
        {viewSolution && <button onClick={handleCloseSolution} className="button">CLOSE SOLUTION</button>}
      </div>
    </div>
  );
};

export default PuzzleTemplate;