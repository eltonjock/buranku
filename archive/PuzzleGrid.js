import React, { useState, useEffect } from 'react';
import puzzleVariables from './variables';

function ClickableSquare({ col, row, digit, color, setColors, showGridLines, gridSize, squareSize }) {
  const isWithinGridSize = row > (20 - gridSize) && col > (20 - gridSize);

  const updateColor = () => {
    if (isWithinGridSize) {
      setColors((prevColors) => {
        const newColors = [...prevColors];
        const nextColor = color === 'white' ? '#e0e0e0' : color === '#e0e0e0' ? 'black' : 'white';
        newColors[row - 1][col - 1] = nextColor;
        return newColors;
      });
    }
  };

  const style = {
    width: `${squareSize}px`,
    height: `${squareSize}px`,
    border: '1px solid black',
    borderColor: showGridLines && isWithinGridSize ? 'black' : 'transparent',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: color,
  };

  const textStyle = {
    margin: 0,
    fontSize: `${squareSize * 0.5}px`,
  };

  return (
    <div style={style} onClick={updateColor}>
      {digit && <p style={textStyle}>{digit}</p>}
    </div>
  );
}

function Grid({ grid, setGrid, colors, setColors, outlines, resetKey }) {
  const totalGridSize = 20;
  const visibleGridSize = puzzleVariables.gridSize;
  const [squareSize, setSquareSize] = useState(40);

  const columnLabels = Array.from({ length: totalGridSize }, (_, i) => String.fromCharCode(65 + i));
  const [clues, setClues] = useState(puzzleVariables.clues);

  useEffect(() => {
    clues.forEach(clue => {
      const { colLabel, row, digit } = clue;
      const colIndex = columnLabels.indexOf(colLabel);
      setGrid((prevGrid) => {
        const newGrid = [...prevGrid];
        newGrid[row - 1][colIndex] = digit;
        return newGrid;
      });
    });
  }, [clues, columnLabels, setGrid]);

  useEffect(() => {
    setClues(puzzleVariables.clues);
  }, [resetKey]);

  useEffect(() => {
    const handleResize = () => {
      const gridContainer = document.querySelector('.gridContainer');
      if (gridContainer) {
        const containerWidth = gridContainer.clientWidth;
        const containerHeight = gridContainer.clientHeight;
        const minDimension = Math.min(containerWidth, containerHeight);
        const newSquareSize = Math.floor(minDimension / (totalGridSize + 2));
        setSquareSize(newSquareSize);
      }
    };

    handleResize();
    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, [totalGridSize]);

  const renderGrid = () => {
    return (
      <div style={{ width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <div style={{ display: 'grid', gridTemplateColumns: `repeat(${totalGridSize}, ${squareSize}px)`, gridTemplateRows: `repeat(${totalGridSize}, ${squareSize}px)`, margin: `${squareSize}px` }}>
          {grid.map((row, rowIndex) => (
            row.map((digit, colIndex) => (
              <ClickableSquare
                key={`${columnLabels[colIndex]}${rowIndex + 1}-${resetKey}`}
                col={colIndex + 1}
                row={rowIndex + 1}
                digit={digit}
                color={colors[rowIndex][colIndex]}
                setColors={setColors}
                showGridLines={outlines[rowIndex][colIndex]}
                gridSize={visibleGridSize}
                squareSize={squareSize}
              />
            ))
          ))}
        </div>
      </div>
    );
  };

  return <div className="gridContainer" style={{ width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', overflow: 'visible' }}>{renderGrid()}</div>;
}

export { ClickableSquare, Grid as PuzzleGrid};