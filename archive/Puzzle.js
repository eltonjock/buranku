// src/Puzzle.js
import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { db } from './firebase-config';

const Puzzle = () => {
  const { versionNumber } = useParams();
  const [puzzle, setPuzzle] = useState(null);

  useEffect(() => {
    const fetchPuzzle = async () => {
      const snapshot = await db.collection('puzzles').doc('BURANKU').collection('versions')
                         .where('versionNumber', '==', parseInt(versionNumber, 10)).get();
      if (!snapshot.empty) {
        setPuzzle(snapshot.docs[0].data());
      }
    };

    fetchPuzzle();
  }, [versionNumber]);

  if (!puzzle) {
    return <div>Loading...</div>;
  }

  return (
    <div>
      {/* Render your puzzle here based on the fetched data */}
      <h2>Puzzle {puzzle.versionNumber}</h2>
      {/* More puzzle details */}
    </div>
  );
};

export default Puzzle;

